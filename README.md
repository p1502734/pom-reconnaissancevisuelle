# POM - Reconnaissance visuelle de figurines articulées



## Cahier des charges

#### Contexte

L'ensemble des sujets proposés par Mr GUILLOU ont pour but de fonctionner ensemble,
leur objectif est de rendre un jeu de plateau interactif en utilisant différents aspects de l'informatique
(reconnaissance visuelle, réalité augmentée, simulation/animation 3D). 
Notre sujet traite de la reconnaissance des différentes figurines présentes sur le plateau.
A partir d'une caméra filmant le plateau de jeu, chaque figurine sera identifiée.
Ensuite un vidéo-projecteur pourra indiquer les déplacements possibles de chaque figurine.
De plus, en connaissant les positions relatives de chaque figurines, 
il est possible de simuler le champ de vision afin de permettre à un joueur de se mettre "à la place" de la figurine.

-----------------

Dans le but d'assurer une reconnaissance fiable des figurines (seuil à définir),
nous avons choisi d'opter pour une approche machine learning. Pour celà, nous avons
besoin de définir un jeu de données qui servira de base d'apprentissage à notre programme.

Cette base doit être suffissament conséquente pour assurer un taux de reconnaissance
satifaisant. Concernant, la collecte des données on peut soit :

* Effectuer une capture vidéo avec une caméra et segmenter la capture en plusieurs
plans signicatifs. On pourra ensuite traiter ces images de façon à marquer la position
des zones articulées, de façon à ce que notre IA puisse renconnaitre une figurine même
si elle n'est pas présentée dans cette position dans sa base de connaissance.

* Effectuer une capture vidéo avec un capteur binoculaire pour pouvoir générer un
mesh 3D de chaque figurine. Le comportement de l'IA serait similaire à celui décrit
dans le point ci-dessus mais avec un jeu de données différent.
