#include <zmq.hpp>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <json/json.h>

#include <sys/types.h>
#include <sys/wait.h>



#define SZ_STRING			(100)

int spawn (char* program, char** arg_list, std::vector<std::string>& messages)
{
    pid_t child_pid;

    //Création d'un pipe entre le père et le fils
    int fd[2];
    pipe(fd);

    //int status;

    //Buffer de lecture du pipe
    char* readBuf= new char[SZ_STRING];

    /* Duplique ce processus. */
    child_pid = fork ();
    if (child_pid != 0) {

        //On ferme l'accès en écriture du pipe côté père
        close(fd[1]);
        /* Nous sommes dans le processus parent. */

        //waitpid(child_pid, &status, 0);

        //On lit sur le pipe
        read(fd[0], readBuf, SZ_STRING);
        std::string toAdd(readBuf);
        messages.push_back(toAdd);
        return child_pid;
    }else {
        //On ferme l'accès en lecture du pipe côté fils et on paramètre l'écriture sur la srotie standard
        dup2(fd[1], STDOUT_FILENO);
        close(fd[0]);

        /* Exécute PROGRAM en le recherchant dans le path. */
        execvp (program, arg_list);
        /* On ne sort de la fonction execvp uniquement si une erreur survient. */
        fprintf (stderr, "une erreur est survenue au sein de execvp\n");
        abort ();
    }
}

int main(int argc, char **argv) {

    if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << "./Interface fSVG fTensor" << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }

    if (argc > 3) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << "./Interface fSVG fTensor" << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }

    //On lance les processus "fils"

    std::vector<std::string> messages;

    char * arg_list_svg[]  = {argv[1], "SVG", nullptr};

    char * arg_list_tensor[] = {argv[2], "Tensor", nullptr};

    spawn (argv[1], arg_list_svg, messages);

    spawn (argv[2], arg_list_tensor, messages);

    for(int i = 0; i < messages.size(); i++) std::cout << messages[i] << std::endl;

    //Parsage des JSON reçu
    Json::Value rootSVG;
    Json::Value rootTensor;
    std::string errorsSVG, errorsTensor;
    Json::CharReaderBuilder builder;
    Json::CharReader *reader = builder.newCharReader();

    auto okSVG = reader->parse(messages[0].c_str(), messages[0].c_str() + messages[0].size(), &rootSVG, &errorsSVG);
    auto okTensor = reader->parse(messages[1].c_str(), messages[1].c_str() + messages[1].size(), &rootTensor, &errorsTensor);


    if (!okSVG) {
        std::cout <<"SVG : Error while parsing : " << errorsSVG << std::endl;
        return EXIT_FAILURE;
    } else if(!okTensor) {
        std::cout <<"Tensor :  Error while parsing : " << errorsTensor << std::endl;
        return EXIT_FAILURE;
    }else {
        std::cout<<"Successfully parsed !!" <<std::endl;
    }

    std::cout << rootSVG << std::endl;

    std::cout << rootTensor << std::endl;

    //TODO Traitrement des messages reçus et interpolation

    return 0;
}