cmake_minimum_required(VERSION 3.13)
project(Interface)

set(CMAKE_CXX_STANDARD 17)

## load in pkg-config support
find_package(PkgConfig)

set(SOURCE_FILES main.cpp)

pkg_check_modules(JSONCPP jsoncpp)
link_libraries(${JSONCPP_LIBRARIES})

add_executable(Interface ${SOURCE_FILES})

target_include_directories(Interface PUBLIC ${JSONCPP_INCLUDE_DIRS})
target_link_libraries(Interface PUBLIC ${JSONCPP_LIBRARIES})