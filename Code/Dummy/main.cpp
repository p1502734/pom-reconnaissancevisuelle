#include <iostream>
#include <zmq.hpp>
#include <json/json.h>

int main(int argc, char **argv) {

    if (argc < 1) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << "./Dummy procName" << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }

    if (argc > 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << "./Dummy procName" << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }

    std::string procId(argv[1]);
    std::string strJson= "{\"Id\":\"" + procId + "\"}";

    fprintf(stdout, "%s", strJson.c_str());

    return 0;
}